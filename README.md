# Friends Of GitLab

## Self-Managed Docker Setup

This is an MIT-licensed open source project with its ongoing development made possible by the support of the community.

## Table of Contents
- [Requirements](#Requirements)
- [Installation](#Installation)
- [Configuration](#Configuration)

<a name="Requirements"></a>
## Requirements

- Debian 10+
- [Apache Web Server](https://www.digitalocean.com/community/tutorials/how-to-install-the-apache-web-server-on-debian-10)
- [Docker](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-debian-10)
- [Let's Encrypt](https://www.digitalocean.com/community/tutorials/how-to-secure-apache-with-let-s-encrypt-on-debian-10)

<a name="Installation"></a>
## Installation

Update the `.env` file to meet your requirements, then run:
```sh
  cd /opt/src
  ./gitlab.sh
```

This will download and start a GitLab container and publish the HTTP port. All GitLab data will be stored as subdirectories of ${GITLAB_HOME}. The container will automatically restart after a system reboot.

<a name="Configuration"></a>
## Configuration

1. Configure [Apache Web Server](https://www.digitalocean.com/community/tutorials/how-to-install-the-apache-web-server-on-debian-10):
    - Make sure the following modules are activated:
        - alias_module
        - suexec_module
        - proxy_module
        - ssl_module

    - Create your virtual host configuration file:
        ```sh
        sudo nano /etc/apache2/sites-available/gitlab.example.com.conf
        ```

    - Paste in the following configuration block changing `hostmaster@example.com` and `gitlab.example.com`:
        ```
        <VirtualHost *:80>
            SuexecUserGroup "#33" "#33"

            ServerAdmin hostmaster@example.com
            ServerName gitlab.example.com

            CustomLog ${APACHE_LOG_DIR}/gitlab.example.com_access_log combined
            ErrorLog ${APACHE_LOG_DIR}/gitlab.example.com_error_log

            RedirectMatch ^/(?!.well-known)(.*)$ https://gitlab.example.com/$1
        </VirtualHost>
        ```

2. Configure [Let's Encrypt](https://www.digitalocean.com/community/tutorials/how-to-secure-apache-with-let-s-encrypt-on-debian-10) Certificate:
    - Obtain an SSL Certificate
        ```sh
        sudo certbot --apache -d gitlab.example.com
        ```

    - Edit your virtual host configuration file:
        ```sh
        sudo nano /etc/apache2/sites-available/gitlab.example.com-le-ssl.conf
        ```

    - Replace in the following configuration block changing `hostmaster@example.com`, `gitlab.example.com`, and `10080`:
        ```
        <IfModule mod_ssl.c>
            <VirtualHost *:443>
                SuexecUserGroup "#33" "#33"

                ServerAdmin hostmaster@example.com
                ServerName gitlab.example.com

                CustomLog ${APACHE_LOG_DIR}/gitlab.example.com_access_log combined
                ErrorLog ${APACHE_LOG_DIR}/gitlab.example.com_error_log

                AllowEncodedSlashes On
                ProxyPass / http://127.0.0.1:10080/ retry=0
                ProxyPassReverse / http://127.0.0.1:10080/
                ProxyPreserveHost On
                ProxyRequests Off

                Include /etc/letsencrypt/options-ssl-apache.conf
                SSLCertificateFile /etc/letsencrypt/live/gitlab.example.com/fullchain.pem
                SSLCertificateKeyFile /etc/letsencrypt/live/gitlab.example.com/privkey.pem
                SSLEngine on
                SSLProxyEngine on
            </VirtualHost>
        </IfModule>
        ```

3. Configure GitLab:
    - Visit the GitLab URL, and sign in with the username root and the password from the following command:
        ```sh
        sudo docker exec -it gitlab grep 'Password:' /etc/gitlab/initial_root_password
        ```

    - Go to Admin / Settings / General and expand **Visibility and access controls** section:
        - **Enabled Git access protocols** = `Only HTTP(S)`
        - **Custom Git clone URL for HTTP(S)** = `https://gitlab.example.com` (replace `gitlab.example.com` with the name of your host)

4. Read [GitLab Docs](https://docs.gitlab.com/ee/install/docker.html) for more information.


----------

#### Credits
1. https://docs.gitlab.com/ee/install/docker.html
2. https://www.digitalocean.com/community/tutorials/how-to-install-the-apache-web-server-on-debian-10
3. https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-debian-10
4. https://www.digitalocean.com/community/tutorials/how-to-secure-apache-with-let-s-encrypt-on-debian-10

----------

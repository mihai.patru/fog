# How to contribute
We welcome everyone to submit pull requests with:
- issue fixes
- change suggestions
- documentation updates

However, not every pull request will automatically be accepted. I will review each carefully to make sure it is in line with
 the direction I want the package to continue in. This might mean that some pull requests are not accepted, or might stay
 unmerged until a place for them can be determined.

## Testing
- [ ] When fixing errors write and satisfy new unit tests that replicate the issue.
- [ ] Make sure there are no errors.

## Submitting changes
When submitting a pull request, it is important to make sure to complete the following:
- [ ] Add a descriptive header that explains in a single sentence what problem the PR solves.
- [ ] Explain why you think it should be implemented one way vs. another, highlight performance improvements, etc.

## Coding conventions
Start reading our code and you'll get the hang of it. We optimize for readability:
- indent using four spaces (soft tabs)
- this is open source software. Consider the people who will read your code, and make it look nice for them. It's sort of like driving a car: Perhaps you love doing donuts when you're alone, but with passengers the goal is to make the ride as smooth as possible.
- emphasis readability of code over patterns to reduce mental debt

Thanks!

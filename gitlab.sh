#!/bin/sh

if [ -f .env ]; then
    export $(echo $(cat .env | sed 's/#.*//g'| xargs) | envsubst)
fi

docker pull gitlab/${GITLAB_DIST}:latest
docker stop gitlab
docker rm gitlab
docker run \
    --detach \
    --hostname ${GITLAB_HOST} \
    --publish ${GITLAB_PORT}:80 \
    --name ${GITLAB_NAME} \
    --restart always \
    --volume ${GITLAB_HOME}/etc:/etc/gitlab \
    --volume ${GITLAB_HOME}/var/log:/var/log/gitlab \
    --volume ${GITLAB_HOME}/var/opt:/var/opt/gitlab \
    --shm-size 256m \
    gitlab/${GITLAB_DIST}:latest
